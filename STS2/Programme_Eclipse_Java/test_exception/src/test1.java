
public class test1 {

	public void test1() {
		// TODO Auto-generated constructor stub


	}

	

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		GastonLagaffe gaston = new GastonLagaffe();
		//System.out.println("Debout Gaston ! Il faut trier le courrier ! ");
		//gaston.test1(20);
		//System.out.println("Gaston, il est temps de rager votre bureau !");
		//gaston.rangerBureau();
		//System.out.println("Gaston, Mr. Demesmaeker arrive, faites vite !");
		//gaston.faireSignerContrats();
		//appeler(gaston, "Mr. Boulier");
		//appeler(gaston, "Prunelle");
		//appeler(gaston, "toto");
		//preparerJournal(gaston);
		try
		{
			gaston.preparerCafe("Moiselle Jeanne");
			gaston.preparerCafe("Lebrac");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	
	}

	private static void appeler(GastonLagaffe gaston, String appelant)
	{
		System.out.println("Gaston, " + appelant + " au téléphone !");
		try
		{
			gaston.repondreAuTelephone(appelant);
		}
		catch(Exception e)
		{
			System.out.println("Encore une bonne excuse, j'imagine ?");
			System.out.println(e.getMessage());
		}
	}

		
	private static void preparerJournal(GastonLagaffe gaston)
	{
		System.out.println("Gaston, une commande urgente !");
		try
		{
			gaston.commanderFournitures();
		}
		catch(Exception e)
		{
			System.out.println("Gaston, d'où vient cette d'odeur ?");
			System.out.println(e.getMessage());
		}
	}
}
